import React, {useState} from 'react';
import {Jumbotron, Container} from 'react-bootstrap'
import Notes from "./Notes";
import AddNote from "./Addnote";

function App() {
  const [Noteslist, editNotes] = useState([{
    bg: 'info',
    name: 'recordatorio',
    desc: 'go to medic'
  }]);

  return (
    <div>
      <Jumbotron fluid>
          <Notes data={Noteslist}></Notes>

        <Container>
          <AddNote data={Noteslist} change={editNotes}></AddNote>
        </Container>
      </Jumbotron>
    </div>
  );
}

export default App;