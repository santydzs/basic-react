import React, {useState} from 'react';
import {Modal, Button, InputGroup, FormControl} from 'react-bootstrap'

function AddNote(props){
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    let title = '';
    let desc = '';

    let updatetitle = (newvalue) =>{
        title = newvalue.target.value;
    }
    let updatedesc = (newvalue) =>{
        desc = newvalue.target.value;
    }

    const saveNote = () =>{
        props.data.push({bg: 'info', name: title, desc: desc});
        props.change(Object.assign([], props.data));
        handleClose();
    }

    return(
        <div>
            <Button variant="primary" onClick={handleShow}>Agregar Nota</Button>

            <Modal show={show} onHide={handleClose}>
            <Modal.Body>
                <InputGroup size="sm" className="mb-3">
                    <FormControl placeholder="Titulo" onChange={updatetitle} aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                    <FormControl placeholder="Descripcion" onChange={updatedesc} aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>Cerrar</Button>
                <Button variant="primary" type="submit" onClick={saveNote}>Guardar Nota</Button>
            </Modal.Footer>
            </Modal>
        </div>
    )
};

export default AddNote;