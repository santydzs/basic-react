import React from 'react';
import {Card, CardColumns} from 'react-bootstrap'

function Notes(props){
    return(
      <CardColumns>
        {props.data.map((Note) =>
          <Card bg={Note.bg} text="dark" style={{ width: '18rem', margin: '5px'}}>
            <Card.Header>{Note.name}</Card.Header>
            <Card.Body>
              <Card.Text>
                {Note.desc}
              </Card.Text>
            </Card.Body>
          </Card>
        )}
      </CardColumns>
    )
};

export default Notes;